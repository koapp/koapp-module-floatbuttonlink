(function() {
  'use strict';
  angular
    .module('floatbuttonlink', [])
    .controller('floatbuttonlinkController', loadFunction);
  loadFunction.$inject = ['$scope', 'structureService', '$location'];
  function loadFunction($scope, structureService, $location) {
    // Register upper level modules
    structureService.registerModule($location, $scope, 'floatbuttonlink');
    // --- Start floatbuttonlinkController content ---
    var config = $scope.floatbuttonlink.modulescope;
    var items  = config.menuItems;
    var modules;


    let top    = config.top    === 0? "auto": config.top    +   "%";
    let bottom = config.bottom === 0? "auto": config.bottom +   "%";
    let left   = config.left   === 0? "auto": config.left   +   "%";
    let right  = config.right  === 0? "auto": config.right  +   "%";


    $scope.menuStyle = {
      "top": top,
      "bottom": bottom,
      "left": left,
      "right": right
    };


    //console.log(config, $scope.menuStyle);
    
    $scope.module = getModules();
    function getModules() {
      modules = [];
      function processChild(value, index) {
        structureService.getModule(value.path).then(function(module) {
          modules.push({
            text: module.name,
            icon: module.icon,
            url: '#' + value.path
          });
        });
      }
      angular.forEach(items, processChild);
      return modules;
    }
  }
}());